SET 'sql-client.execution.result-mode' = 'tableau';
SET 'execution.runtime-mode' = 'streaming';
ADD JAR 'file:///home/tchin/flink-connector-kafka-1.15.2.jar';
ADD JAR 'file:///home/tchin/kafka-clients-2.4.1.jar';
CREATE FUNCTION get_images AS 'stateless_table.get_images' LANGUAGE PYTHON;

CREATE TABLE PageCreate (
   page_id BIGINT,
   page_title STRING
) WITH (
  'connector' = 'kafka',
  'topic' = 'eqiad.mediawiki.page-create',
  'properties.bootstrap.servers' = 'kafka-jumbo1001.eqiad.wmnet:9092',
  'properties.group.id' = 'testGroup',
  'format' = 'json',
  'scan.startup.mode' = 'latest-offset'
);

CREATE TABLE TestTopic (
   images ARRAY<ROW<ns INT, title STRING>>
)WITH (
  'connector' = 'kafka',
  'topic' = 'tchin_test0',
  'properties.bootstrap.servers' = 'kafka-jumbo1001.eqiad.wmnet:9092',
  'properties.group.id' = 'testGroup',
  'format' = 'json',
  'scan.startup.mode' = 'latest-offset'
);

INSERT INTO TestTopic SELECT get_images(page_id) FROM PageCreate;
