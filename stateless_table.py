from pyflink.java_gateway import get_gateway
from pyflink.datastream import StreamExecutionEnvironment
from pyflink.table import DataTypes, TableDescriptor, StreamTableEnvironment
from pyflink.table.udf import udf
import requests
import json


def flink_jvm():
    return get_gateway().jvm


@udf(input_types=DataTypes.BIGINT(), result_type=DataTypes.STRING())
def get_images(page_id: int):
    response = requests.get(
        f"https://en.wikipedia.org/w/api.php?action=query&format=json&prop=images&pageids={page_id}")
    if response.status_code == 200:
        try:
            return json.dumps(response.json()['query']['pages'][str(page_id)]['images'])
        except KeyError:
            pass
    return json.dumps([])


if __name__ == "__main__":
    schema_uris = [
        "https://schema.wikimedia.org/repositories/primary/jsonschema",
        "https://schema.wikimedia.org/repositories/secondary/jsonschema",
    ]
    stream_config_uri = "https://meta.wikimedia.org/w/api.php"

    env = StreamExecutionEnvironment.get_execution_environment()

    env.add_jars("file:///home/tchin/eventutilities-flink-1.2.0-jar-with-dependencies.jar",
                 "file:///home/tchin/flink-connector-kafka-1.15.2.jar",
                 "file:///home/tchin/kafka-clients-3.2.3.jar")

    env.set_parallelism(1)

    st_env = StreamTableEnvironment.create(env)

    EventTableDescriptorBuilder = flink_jvm().org.wikimedia.eventutilities.flink.table.EventTableDescriptorBuilder

    EventTableDescriptorBuilderFrom = getattr(EventTableDescriptorBuilder, 'from')
    tableDescriptorBuilder = EventTableDescriptorBuilderFrom(schema_uris, stream_config_uri)

    page_create_stream_table_descriptor = TableDescriptor(
        tableDescriptorBuilder.eventStream(
            "mediawiki.page-create"
        ).setupKafka(
            "kafka-jumbo1001.eqiad.wmnet:9092",
            "my_consumer_group",
        ).withKafkaTimestampAsWatermark().option("json.timestamp-format.standard", "ISO-8601").build()
    )
    page_create_stream_table = st_env.from_descriptor(page_create_stream_table_descriptor)

    st_env.create_temporary_view("mediawiki_page_create", page_create_stream_table)
    st_env.create_temporary_function("get_images", get_images)

    result_table = st_env.sql_query(
        """
        SELECT page_title, get_images(page_id) as images FROM mediawiki_page_create
        """
    )

    # Print the results on the CLI.
    streaming_result = result_table.execute()
    streaming_result.print()
