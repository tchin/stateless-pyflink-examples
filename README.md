# Stateless PyFlink Examples

Example pipelines using PyFlink and `wikimedia-eventutilities-flink`.

The pipelines read from `mediawiki.page-create` Kafka topic and fetches the list of images on the page using the action api.

Unlike normal Flink, PyFlink does not have `AsyncDataStream` or `AsyncTableFunction`. User-defined functions are process asynchronously and [configured](https://nightlies.apache.org/flink/flink-docs-release-1.15/docs/dev/python/python_config/) with `python.fn-execution.bundle.size`.

## Datastream


### Sample Output

```
Row(page_title='File:NLC892-411999027560-49817_劉子威集_第3冊.pdf', images=[])
Row(page_title='Cristobal_de_la_Sierra', images=[{'ns': 6, 'title': 'File:Camera-photo.svg'}, {'ns': 6, 'title': 'File:Rhacodactylus ciliatus.jpg'}])
Row(page_title='Категорија:Шефови_влада', images=[])
```

## Table


### Sample Output

```
+----+--------------------------------+--------------------------------+
| op |                     page_title |                         images |
+----+--------------------------------+--------------------------------+
| +I | California_production_of_peach |                             [] |
| +I |   Kaozeal:Charlez_I_a_Vro-Saoz | [{"ns": 6, "title": "File:C... |
| +I | Kategorie:Postaveno_v_Němec... |                             [] |
```

## [WIP] FlinkSQL + Python UDF

### Steps

1. Download Necessary Dependencies
```bash
wget https://repo1.maven.org/maven2/org/apache/flink/flink-connector-kafka/1.15.2/flink-connector-kafka-1.15.2.jar
wget https://repo1.maven.org/maven2/org/apache/kafka/kafka-clients/2.4.1/kafka-clients-2.4.1.jar
wget https://repo1.maven.org/maven2/org/apache/flink/flink-python_2.12/1.15.2/flink-python_2.12-1.15.2.jar
```

2. Start cluster
```bash
./bin/start-cluster.sh
```

3. Start SQL Client with necessary files. Currently it's using Anaconda python3.7 to solve Python dependencies. Change the file paths as necessary
```bash
./bin/sql-client.sh -pyexec /usr/lib/anaconda-wmf/bin/python3.7 -pyclientexec /usr/lib/anaconda-wmf/bin/python3.7 -pyfs ../stateless_table.py -j ../flink-python_2.12-1.15.2.jar
```

4. Run the SQL in `stateless_sql_udf.sql`


5. Remember to stop cluster when done
```bash
./bin/stop-cluster.sh
```
