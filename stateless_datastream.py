from pyflink.java_gateway import get_gateway
from pyflink.datastream import StreamExecutionEnvironment, MapFunction
from pyflink.datastream.connectors import Source
from pyflink.common import WatermarkStrategy, Row
import requests


def flink_jvm():
    return get_gateway().jvm


def get_images(page_id: int):
    response = requests.get(
        f"https://en.wikipedia.org/w/api.php?action=query&format=json&prop=images&pageids={page_id}")
    if response.status_code == 200:
        try:
            return response.json()['query']['pages'][str(page_id)]['images']
        except KeyError:
            pass
    return []


class PagesToImages(MapFunction):
    def map(self, value):
        page_id = value['page_id']
        return Row(page_title=value['page_title'], images=get_images(page_id))


if __name__ == "__main__":
    schema_uris = [
        "https://schema.wikimedia.org/repositories/primary/jsonschema",
        "https://schema.wikimedia.org/repositories/secondary/jsonschema",
    ]
    stream_config_uri = "https://meta.wikimedia.org/w/api.php"

    env = StreamExecutionEnvironment.get_execution_environment()
    env.add_jars("file:///home/tchin/eventutilities-flink-1.2.0-jar-with-dependencies.jar",
                 "file:///home/tchin/flink-connector-kafka-1.15.2.jar",
                 "file:///home/tchin/kafka-clients-3.2.3.jar")

    env.set_parallelism(1)
    EventDataStreamFactory = flink_jvm().org.wikimedia.eventutilities.flink.stream.EventDataStreamFactory

    EventDataStreamFactoryFrom = getattr(EventDataStreamFactory, 'from')
    datastream_factory = EventDataStreamFactoryFrom(schema_uris, stream_config_uri)

    kafka_source = Source(
        datastream_factory.kafkaSourceBuilder(
            "mediawiki.page-create",
            "kafka-jumbo1001.eqiad.wmnet:9092",
            "test_tchin_0"
        ).build()
    )

    datastream = env.from_source(
        source=kafka_source,
        watermark_strategy=WatermarkStrategy.for_monotonous_timestamps(),
        source_name="kafka_mediawiki_page_create"
    )

    datastream.map(PagesToImages()).print()
    env.execute()
